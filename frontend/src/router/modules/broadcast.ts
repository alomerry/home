import { $t } from "@/plugins/i18n";
import { broadcast } from "@/router/enums";

export default {
  path: "/broadcast",
  redirect: "/broadcast/live",
  meta: {
    title: $t("menus.broadcast"),
    icon: "ic:twotone-broadcast-on-personal",
    rank: broadcast
  },
  children: [
    {
      path: "/broadcast/live",
      name: "BroadcastLive",
      component: () => import("@/views/broadcast/live/index.vue"),
      meta: {
        title: $t("menus.broadcastLive"),
        keepAlive: true
      }
    },
    {
      path: "/broadcast/videos",
      name: "BroadcastVideo",
      component: () => import("@/views/broadcast/video/index.vue"),
      meta: {
        title: $t("menus.broadcastVideo"),
        keepAlive: true
      }
    }
  ]
} as RouteConfigsTable;
