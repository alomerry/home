package utils

import (
	"crypto/md5"
	"encoding/hex"
	"io"
)

func Md5(origin string) string {
	h := md5.New()
	io.WriteString(h, origin)
	return hex.EncodeToString(h.Sum(nil))
}
