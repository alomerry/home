package component

import (
	core "github.com/alomerry/home/core/env"
	steam "github.com/alomerry/steam-web-go-api"
)

var Client *steam.SteamClient

func init() {
	if Client == nil {
		Client = steam.NewClient(core.GetSteamWebAPI())
	}
	//Client.GetIUserStats().GetUserStatsForGame()
}
