package mgo

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/event"
	"log"
)

func NewCommandMonitor() *event.CommandMonitor {
	return &event.CommandMonitor{
		Started: func(c context.Context, cse *event.CommandStartedEvent) {
			log.Println(fmt.Sprintf("%v", cse.Command))
		},
		Succeeded: func(c context.Context, cse *event.CommandSucceededEvent) {

		},
		Failed: func(c context.Context, cfe *event.CommandFailedEvent) {
		},
	}
}
