package mgo

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/bsoncodec"
)

var DefaultRegistry *bsoncodec.Registry = nil

func init() {
	rb := bsoncodec.NewRegistryBuilder()
	bsoncodec.DefaultValueEncoders{}.RegisterDefaultEncoders(rb)
	bsoncodec.DefaultValueDecoders{}.RegisterDefaultDecoders(rb)

	//decoder
	//encoder

	var primitiveCodecs bson.PrimitiveCodecs
	primitiveCodecs.RegisterPrimitiveCodecs(rb)
	DefaultRegistry = rb.Build()
}
