package extension

import (
	"context"
	"github.com/alomerry/home/core/extension/mgo"
	"github.com/qiniu/qmgo"
	qopt "github.com/qiniu/qmgo/options"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

var (
	debug  bool
	client *qmgo.QmgoClient
)

func init() {
	config := &qmgo.Config{
		Uri:      "mongodb://admin@dev:27017/?authSource=admin",
		Database: "home",
	}

	opts := options.Client()
	opts.SetAppName("home")
	opts.SetSocketTimeout(1 * time.Minute)
	opts.SetMaxConnIdleTime(1 * time.Minute)
	opts.SetConnectTimeout(5 * time.Second)
	opts.SetServerSelectionTimeout(2 * time.Second)
	opts.SetMaxPoolSize(4096)
	if debug {
		opts.SetMonitor(mgo.NewCommandMonitor())
	}
	opts.Registry = mgo.DefaultRegistry
	opts.Registry = bson.DefaultRegistry

	var err error
	client, err = qmgo.Open(context.Background(), config, qopt.ClientOptions{
		ClientOptions: opts,
	})
	if err != nil {
		panic(err)
	}

}
