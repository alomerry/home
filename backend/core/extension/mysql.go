package extension

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"os"
)

var (
	timeout = "10s" // 连接超时，10秒
	db      *gorm.DB
)

func _init() {
	// refer https://github.com/go-sql-driver/mysql#dsn-data-source-name for details

	var (
		err error
		dsn = os.Getenv("WATER_MYSQL_DSN")
	)
	dsn = "home:Eb7zKia5faj2kPL6@tcp(bt.alomerry.com:3306)/home"
	dsn = fmt.Sprintf("%s?charset=utf8&parseTime=True&loc=Local&timeout=%s", dsn, timeout)
	db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	// db = db.Set("gorm:table_options", "ENGINE=InnoDB CHARSET=utf8 auto_increment=1")
	sqlDB, err := db.DB()
	if err != nil {
		panic(err)
	}
	sqlDB.SetMaxOpenConns(100) // 设置数据库连接池最大连接数
	sqlDB.SetMaxIdleConns(20)  // 连接池最大允许的空闲连接数，如果没有sql任务需要执行的连接数大于20，超过的连接会被连接池关闭。
}

func _GetDB() *gorm.DB {
	return db
}
