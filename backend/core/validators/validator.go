package validators

import (
	"errors"
	"github.com/asaskevich/govalidator"
)

func ValidateRequest(req interface{}) error {
	if _, err := govalidator.ValidateStruct(req); err != nil {
		// TODO
		return errors.New("xxx")
	}

	return nil
}
