#!/usr/bin/env bash

HOME_PATH=$(
  cd $(dirname ${0})
  pwd
)

SERVICE_PATH=${HOME_PATH}/service
PROTO_PATH=${HOME_PATH}/proto

main() {
  case "$1" in
  service)
    "${SERVICE_PATH}"/scripts/build ${@:2}
    ;;
  gateway)
    "${PROTO_PATH}"/gen-stub ${@:2}
    ;;
  proto)
    "${PROTO_PATH}"/gen-stub ${@:2}
    ;;
  cron)
    "${CRON_PATH}"/scripts/build ${@:2}
    ;;
  openapi)
    "${BUSINESS_PATH}"/scripts/build ${@:2}
    ;;
  *)
    usage
    ;;
  esac
}

main $@
