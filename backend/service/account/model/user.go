package model

import (
	"context"
	"errors"
	"github.com/alomerry/home/core/extension"
	"gorm.io/gorm"
	"time"
)

var (
	CUser = &User{}
)

const (
	StatusValid string = "1"
	StatusLock  string = "0"

	SexMale    string = "0"
	SexFeMale  string = "1"
	SexUnknown string = "2"

	DefaultAvatar string = "default.jpg"
)

func init() {
	_ = extension.GetDB().AutoMigrate(CUser)
}

// User 用户信息表
type User struct {
	ID          uint64 `gorm:"primaryKey;autoIncrement:true"`
	Username    string `gorm:"column:username;type:varchar(10);uniqueIndex"`
	Password    string `gorm:"column:password"`
	DeptId      uint64 // 部门 ID
	DeptName    string // 部门
	Email       string // 邮箱
	Mobile      string // 手机号
	Status      string // 状态 0 锁定，1 有效
	SSex        string // 性别 0 男，1 女，2 保密
	Description string // 个人描述
	Avatar      string // 头像
	RoleName    string // 角色
	RoleId      string
	CreatedAt   time.Time // 创建时间
	UpdatedAt   time.Time // 更新时间
	LastLoginAt time.Time // 最后登录时间
}

func (*User) TableName() string {
	return "t_user"
}

func (u *User) Locked() bool {
	if u.Status == StatusLock {
		return true
	}
	return false
}

func (u *User) Create(ctx context.Context) error {
	db := extension.GetDB().Create(u)
	if db.Error != nil {
		return db.Error
	}
	return nil
}

func (u *User) UpdateLoginTime() error {
	u.LastLoginAt = time.Now()
	db := extension.GetDB().Save(u)
	if db.Error != nil {
		return db.Error
	}
	return nil
}

func (u *User) GetByUserName(ctx context.Context, username string) (*User, error) {
	user := &User{}
	result := extension.GetDB().Where("username = ?", username).Find(user)
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return nil, errors.New("未找到")
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return user, nil
}
