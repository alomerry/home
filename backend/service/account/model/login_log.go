package model

import (
	"github.com/alomerry/home/core/extension"
	"time"
)

var (
	CLoginLog = &LoginLog{}
)

func init() {
	_ = extension.GetDB().AutoMigrate(CLoginLog)
}

// LoginLog 登录记录表
type LoginLog struct {
	ID        uint64    `gorm:"primaryKey;autoIncrement:true"`
	UserName  string    `gorm:"column:username"`
	IP        string    // IP
	CreatedAt time.Time // 创建时间
	UpdatedAt time.Time // 更新时间
	LoginAt   time.Time // 登录时间
}

func (*LoginLog) TableName() string {
	return "t_login_log"
}

func (*LoginLog) Create(username, ip string) error {
	log := &LoginLog{
		UserName: username,
		IP:       ip,
		LoginAt:  time.Now(),
	}
	db := extension.GetDB().Create(log)
	if db.Error != nil {
		return db.Error
	}
	return nil
}
