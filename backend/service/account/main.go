package main

import (
	"github.com/alomerry/home/core/server"
	"github.com/alomerry/home/proto/account"
	"github.com/alomerry/home/service/account/service"
	"google.golang.org/grpc/reflection"
)

func main() {
	s, lis := server.NewServer(8091)
	account.RegisterAccountServiceServer(s, &service.AccountService{})
	// 支持 postman 调用
	reflection.Register(s)
	s.Serve(lis)
}
