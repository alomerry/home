package main

import (
	"github.com/alomerry/home/core/server"
	"github.com/alomerry/home/proto/video"
	"github.com/alomerry/home/service/video/service"
	"google.golang.org/grpc/reflection"
)

func main() {
	s, lis := server.NewServer(8092)
	video.RegisterVideoServiceServer(s, &service.VideoService{})
	// 支持 postman 调用
	reflection.Register(s)
	s.Serve(lis)
}
