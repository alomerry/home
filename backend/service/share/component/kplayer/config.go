package kplayer

import (
	"time"
)

const (
	VideoResourceTypeFile = "file://"
	VideoResourceTypeRtmp = "rtmp://"
	VideoResourceTypeHTTP = "http(s)://"
	VideoResourceTypeFTP  = "ftp://"

	StatusValid string = "1"
	StatusLock  string = "0"

	SexMale    string = "0"
	SexFeMale  string = "1"
	SexUnknown string = "2"

	DefaultAvatar string = "default.jpg"
)

func BasicCfg() *Config {
	cfg := &Config{
		HomeDir:  "/",
		Resource: Resource{

		},
		Play:     Play{},
		Output:   Output{},
		Plugin:   Plugin{},
	}
	return cfg
}

type Config struct {
	HomeDir  string // 程序运行目录
	Resource Resource
	Play     Play
	Output   Output
	Plugin   Plugin
}
type Play struct {
}

type Output struct {
	ReconnectInternal int32 // 重连间隔时间 单位为秒
	OutputList
}

type Plugin struct {
}

type Resource struct {
	SingleLists []SingleVideoResource
	GroupLists  []GroupVideoResource
	DirLists    []DirVideoResource
	Extensions  []string
}

func (r Resource) GetResource() {

}

type SingleVideoResource struct {
	Path   string // 资源文件路径 必填
	Unique string // 自定义资源唯一标识名称
	Seek   int64  // 从资源的多少秒起始播放
	End    int64  // 播放到视频资源的多少秒后停止
}

type GroupVideoResource struct {
	Unique string  // 自定义资源唯一标识名称
	Seek   int64   // 从资源的多少秒起始播放
	End    int64   // 播放到视频资源的多少秒后停止
	Groups []Group // 资源文件路径 必填
}

type Group struct {
	Path           string // 资源文件路径 必填
	MediaType      string `json:"media_type"`      // 资源类型 必填 支持 video 和 audio 两项
	PersistentLoop bool   `json:"persistent_loop"` // 保持持续的循环
}

type DirVideoResource struct {
	Path string // 资源文件路径 必填
}
