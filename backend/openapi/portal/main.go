package main

import (
	"context"
	"fmt"
	"github.com/alomerry/home/openapi/portal/controller"
	"github.com/alomerry/home/openapi/portal/middleware"
	"github.com/alomerry/home/proto"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gopkg.in/tylerb/graceful.v1"
	"net/http"
	"time"
)

func main() {
	var (
		r   = gin.Default()
		mux = newServeMux()
	)

	r.Use(middleware.Cors())
	r.GET("/ping", controller.Ping)
	r.Any("/v0/*rest", func(c *gin.Context) {
		mux.ServeHTTP(c.Writer, c.Request)
	})

	graceful.Run(fmt.Sprintf(":%s", "4376"), 5*time.Second, r)
}

func newServeMux() http.Handler {
	ctx := context.Background()
	logrus.Info(ctx, "Register services")
	serveMux, err := proto.NewGateway(ctx)
	if err != nil {
		panic(err)
	}
	logrus.Info(ctx, "Register services end")
	return serveMux
}
