package middleware

import "github.com/gin-gonic/gin"

var (
	NoAuthActions = []string{
		"/v2/login",
	}
)

type AuthMiddleware struct {
	Version string
	Env     string
}

func (am *AuthMiddleware) Auth() gin.HandlerFunc {
	return nil
}
